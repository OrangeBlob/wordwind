import os
import xml.etree.ElementTree as ETree
from rich import print as rprint


def cut_end(s, num):
    return s[:-num] if num <= len(s) else ''


def print_element(element, count=None):
    if count is None:
        count = [0]
    if element.tag == 'def':
        count[0] += 1
        # Concatenate all text inside the element
        text = ''.join(element.itertext()).strip()
        rprint(f'[blue]{count[0]}[/blue] : {text}')
    for child in element:
        print_element(child, count)


class GcideSearch:
    def __init__(self, gcide_dir):
        self.gcide_dir = gcide_dir

    def find(self, word):
        # Determine the filename based on the first letter of the word
        filename = f'gcide_{word[0].lower()}-entries.xml'
        filepath = os.path.join(self.gcide_dir, filename)

        # Check if the file exists
        if not os.path.exists(filepath):
            return False

        # Parse the XML file
        tree = ETree.parse(filepath)
        root = tree.getroot()

        # Search for the word in the XML file
        found = False
        for entry in root.findall('entry'):
            if entry.get('key') == word:
                print_element(entry)
                print()
                found = True

        return found

    def deep_find(self, word):
        if len(word) > 3 and (word.endswith("ing") or word.endswith("s")
                              or word.endswith("ly") or word.endswith("ed")):
            for i in range(1, 4):
                word_cut = cut_end(word, i)
                if self.find(word_cut):
                    print(f'DEFINITION FOR "{word_cut}" WAS SHOWN INSTEAD.\n')
                    return True
        return False
