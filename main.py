from gcide_search import GcideSearch
from merriam_webster import MeriamWebster


def format_word(s):
    return s.strip().capitalize()


gcide_dict = GcideSearch("./gcide")
mw_dict = MeriamWebster("https://www.merriam-webster.com/dictionary/")


def main():
    word = input("\033[1;35;40m->\033[0m ")
    word = format_word(word)
    if not gcide_dict.find(word) and not gcide_dict.deep_find(word):
        option = input(f'Could not find "{word}" in the local GCIDE library. '
                       f'Use online Merriam Webster dictionary? (Y/n): ')
        if option != "n":
            mw_dict.find(word)
            print()
    print()


while True:
    main()
