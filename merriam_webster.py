# Original code by s20 AKA src4026 (now modified)

import urllib.request
import urllib.parse
from bs4 import BeautifulSoup
from rich import print as rprint
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE


class MeriamWebster:
    def __init__(self, url):
        self.url = url

    def find(self, word):
        print("Retrieving online Merriam Webster definition...")
        # Retrieving the web page and parsing
        try:
            html = urllib.request.urlopen(self.url + word).read()
        except Exception as e:
            rprint("[red]%s" % e)
            rprint("[yellow][i]The word you have entered isn't in the dictionary."
                   " Please try again with the proper word.[/i]")
            return False
        soup = BeautifulSoup(html, 'html.parser')

        # Retrieve the class="dtText"
        tags = soup.find_all(class_="dtText")
        # print(tags, "\n")
        def_list = list()
        for tag in tags:
            def_list.append(tag.get_text())

        def_list = list(dict.fromkeys(def_list))

        for _, definition in enumerate(def_list):
            rprint(_ + 1, definition)
